# WOLVERINE - WIP

import requests
import re
import time
import json
import csv

topcolumns = ['Company', 'phone', 'email', 'city', 'website']

csvLocalFile = csv.writer(open('myLocalFileName.csv','a'))
csvLocalFile.writerow(topcolumns)
sessionCookie = {"covid-suppliers-list--terms-and-conditions-en": "20200911"}

session = requests.Session()

pageNumber = 0  # in this particular case, total is 17
# baseOntarioPPEURL = "https://covid-19.ontario.ca/workplace-ppe-supplier-directory?gloves%5B25%5D=25&field_ont_city_target_id=All&field_ont_manufacture_value=0&page=" + str(pageNumber) + "#directory"

# make dynamic but for now, set to current total (17)
totalNumPages = 17

# while curr page is less than or equal to the total number of pages (converted to int)
while pageNumber <= int(totalNumPages):
    # redeclare pageURL based on curr page
	pageURL = "https://covid-19.ontario.ca/workplace-ppe-supplier-directory?gloves%5B25%5D=25&field_ont_city_target_id=All&field_ont_manufacture_value=0&page=" + str(pageNumber) + "#directory"

    # increments first?? WOLVERINE - Python Q
	pageNumber = pageNumber + 1 # W3 - Python Q - why do you increment first? ROGUE - only personal style. The other option is to use a defined range in your for-loop: for page in range(0, 17): print(page)
	print(str(pageNumber) + "/" + str(totalNumPages))   # same as console.log? prints page number out of total pages ROGUE: Exactly. The print statement is a standard output for keeping track of your work. Frequently used for debugging. Same as console.log()
	
	PageHTML = session.get(pageURL, cookies=sessionCookie).text
	for every_company in re.finditer('<li><div class="views-field views-field-field-ont-tradename">(.+?)</div></div></li>', PageHTML, re.S | re.DOTALL):
		email = re.search('mailto:(.+?)"', every_company.group(1)).group(1)
		try:
			phone = re.search('tel:\+(.+?)">(.+?)<', every_company.group(1)).group(2)
		except:
			phone = "N/A"
		company_and_website = re.search('<div class="views-field views-field-field-ont-tradename"><div class="field-content">(.+?)</div>', every_company.group(0)).group(0)
		try:
			website = re.search('href="(.+?)"', company_and_website).group(1)
		except:
			website = "N/A"
		if website != "N/A":
			company_name = re.search('<a href="(.+?)" target="_blank">(.+?)</a>', company_and_website).group(2).replace('</span>', '').replace('</abbr>', '').replace('<span>', '').replace('<abbr>', '')
			company_name = re.sub('<span(.+?)>', '', company_name)
			company_name = re.sub('<abbr(.+?)>', '', company_name)
		else:
			company_name = re.search('<div class="field-content">(.+?)</div>', company_and_website).group(1).replace('</span>', '').replace('</abbr>', '').replace('<span>', '').replace('<abbr>', '')
			company_name = re.sub('<span(.+?)>', '', company_name)
			company_name = re.sub('<abbr(.+?)>', '', company_name)
		try:
			city_name = re.search('Location</span><div class="field-content">(.+?)</div>', every_company.group(0)).group(1)
		except:
			city_name = "none-listed"
		row_data = [company_name, phone, email, city_name, website]
		print(row_data)
		csvLocalFile.writerow(row_data)

	# PageHTML = session.post(pageURL, cookies=sessionCookie).text
    # oh bloody hell I'm stuck...
    # End goal: convert to CSV with five headers: Company (company_name), Phone (number), Email (email), Location (city), Website (URL)
	# for every_line in re.finditer('<tr class="hovereffect" onclick="location.href = \'/Facility/Details/(.+?)\' ">', PageHTML):
	# 	# facility_ID = every_line.group(1)
	# 	# if facility_ID not in masterResults:
	# 	# 	masterResults[facility_ID] = {"infractions": {}, "total_inspections": 0}
	# 	# report_page = "https://inspections.vcha.ca/Facility/Details/" + facility_ID
	# 	# reportPageHTML = session.get(report_page, cookies=sessionCookie).text

    #     # W3 - this one is challenging - it's actually deeper, in an <a>, followed by <span class="upperclass">, followed by abbr (Ltd., Inc.) where applicable
	# 	company_name = re.search('<div class="views-field views-field-field-ont-tradename">(.+?)</div>', reportPageHTML).group(1)

    #     # W3 - how to search for something in a tag with no class or id? by group? first is phone, second is email
	# 	company_phone = re.search('<span class="views-label views-label-nothing">Contact</span><span class="field-content">(.+?)</span>', reportPageHTML, re.S|re.DOTALL).group(2)
	# 	company_email = re.search('<span class="views-label views-label-nothing">Contact</span><span class="field-content">(.+?)</span>', reportPageHTML, re.S|re.DOTALL).group(2)

    #     # W3 - I think I finally got one - lol. Minus the DOTALL).group part...facepalm
	# 	company_location = re.search('<span class="views-label views-label-field-ont-city">Location</span><div class="field-content">(.+?)</div>', reportPageHTML, re.S|re.DOTALL).group(2)

    #     # W3 - same as company_location - think I got it...minus DOTALL).group part
    #     company_website = re.search('<span class="views-label views-label-field-ont-website">Website</span><div class="field-content">(.+?)</div>')

    #     # here is where I store each result
	# 	masterResults[company]['company_name'] = company_name
	# 	masterResults[company]['company_phone'] = company_phone
	# 	masterResults[company]['company_email'] = company_email
	# 	masterResults[company]['company_location'] = company_location
	# 	masterResults[company]['company_website'] = company_website

    # # writes to file
	# with open('/Users/CBCWEBDEV03/Documents/supermarket-secrets/vancouver_data_formatted_new.json', 'w') as outfile:
	# 	json.dump(masterResults, outfile)
